import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';


export const  CREDENTIALS = {
  apiKey: "AIzaSyDOgPgh8B0sBI1KDMJIOZsN2tEe3TjQupc",
  authDomain: "vocal-53c61.firebaseapp.com",
  databaseURL: "https://vocal-53c61.firebaseio.com",
  projectId: "vocal-53c61",
  storageBucket: "vocal-53c61.appspot.com",
  messagingSenderId: "355393568318",
  appId: "1:355393568318:web:4b52b7c9e57376cb2c1b6f",
  measurementId: "G-8EXK0LHCBZ"
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, 
            IonicModule.forRoot(), 
            AppRoutingModule,
            AngularFireAuthModule,
            AngularFireModule.initializeApp(CREDENTIALS),
          ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})


export class AppModule {}
