import { Component, OnInit } from '@angular/core';
import {  NavController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

export class User {
  email: string;
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public user:User = new User();

  constructor(
    public navCtrl: NavController, 
    public fAuth: AngularFireAuth,
    private router: Router,

  ) { }

  ngOnInit() {
  }

  async login() {
    try {
      var r = await this.fAuth.auth.signInWithEmailAndPassword(
        this.user.email,
        this.user.password
      );
      if (r) {
        console.log("Successfully logged in!");
        this.router.navigate(['/tabs/tab1']);
      }

    } catch (err) {
      console.error(err);
    }
  }
  
}
