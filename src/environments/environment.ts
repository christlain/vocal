// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export var firebaseConfig = {
  apiKey: "AIzaSyDOgPgh8B0sBI1KDMJIOZsN2tEe3TjQupc",
  authDomain: "vocal-53c61.firebaseapp.com",
  databaseURL: "https://vocal-53c61.firebaseio.com",
  projectId: "vocal-53c61",
  storageBucket: "vocal-53c61.appspot.com",
  messagingSenderId: "355393568318",
  appId: "1:355393568318:web:4b52b7c9e57376cb2c1b6f",
  measurementId: "G-8EXK0LHCBZ"
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
